#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform mat4 brickModel;

in vec3 position;
in vec3 color;
in vec3 normal;
out vec3 fcolor;
out vec3 N;
out vec3 pos;

void main() {
  gl_Position = projection * view * model * brickModel* vec4(position, 1);
  fcolor = color;
  pos = vec3(view*model*vec4(position,1));
  N = normalize(vec3(transpose(inverse(view*model))*vec4(normal,0)));
}
