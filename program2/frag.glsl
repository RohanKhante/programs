#version 330

in vec3 fcolor;
in vec3 N;
in vec3 pos;
out vec4 color_out;
//uniform vec3 lightP;				//could pass these uniform variables from glwidget.cpp instead of assigning them in main here
//uniform float shininess;
//uniform vec3 lightP = vec3(3.0f,4.0f,1.0f);
//uniform float shininess = 2.6f;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
	
void main() {
	vec3 ambientReflect = .1 * fcolor;
	vec3 lightCol = vec3(0.8f,0.0f,100.3f);
	float shininess = 2.6f;
	vec3 specular;
	vec3 lightP = vec3(0.3f,10.0f,0.5f);
	lightP = vec3(view*model*vec4(lightP,1));
	vec3 L = normalize(lightP-pos);
	float diffuse = max(0,dot(N,L));
	vec3 R = 2*dot(N,L)*N-L;
	vec3 ambient = .2+ 0.5f*ambientReflect;
	vec3 V = normalize(-pos);
		//only add specular value if diffuse is greater than zero so object does not look shiney from the bottom compared to light
	if(diffuse > 0.0f) {
		specular = vec3(0.7f*pow(max(0,dot(R,V)), shininess), 0.7f*pow(max(0,dot(R,V)), shininess), 0.7f*pow(max(0,dot(R,V)), shininess));
	}
	else{
		specular = vec3(0,0,0);
	}
	specular = specular;// + lightCol;
	//float specular = 0.5f*pow(max(0,dot(R,V)), shininess);
	color_out = vec4(((fcolor * diffuse * specular + ambient) * fcolor), 1);
  //color_out = vec4(vec3(specular), 1);		//debugging stuff
}
